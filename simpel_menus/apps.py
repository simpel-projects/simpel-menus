from django.apps import AppConfig


class SimpelMenusConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'simpel_menus'
    icon = 'menu'
    verbose_name = "Menus"
